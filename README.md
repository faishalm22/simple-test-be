# Recruitment Backend

Aplikasi backend ini bertujuan untuk memanggil endpoint GraphQL untuk mendapatkan data negara (country list dan country detail), dan mengekspos data tersebut dalam bentuk REST API. Aplikasi ini menggunakan Node.js dengan TypeScript dan framework Express.js, serta HTTP client Axios untuk mengambil data dari GraphQL endpoint.

## Requirement

- Bahasa Pemrograman: Node.js (TypeScript)
- Framework: Express.js
- HTTP Client: Axios
- GraphQL Endpoint: [https://countries.trevorblades.com/graphql](https://countries.trevorblades.com/graphql)

## Instalasi

1. Clone repositori ini ke dalam komputer Anda.

2. Buka terminal dan navigasikan ke direktori proyek.

3. Jalankan perintah berikut untuk menginstal semua dependensi yang diperlukan:

   ```bash
   npm install
   ```

4. Jalankan perintah berikut untuk build aplikasi TypeScript:

   ```bash
   npm run build
   ```

5. Setelah build selesai, Anda dapat menjalankan aplikasi dengan perintah:

   ```bash
   npm start
   ```

Aplikasi akan berjalan di `http://localhost:5000`.

## Endpoints

### 1. Mendapatkan Daftar Nama dan Bahasa Semua Negara

- Endpoint: GET http://localhost:5000/countries

- Contoh Response JSON:

   ```json
   {
       "data": {
           "countries": [
               {
                   "name": "Andorra",
                   "languages": [ "Catalan"]
               },
               {
                   "name": "United Arab Emirates",
                   "languages": [ "Arabic" ]
               },
               {
                   "name": "Afghanistan",
                   "languages": ["Pashto", "Uzbek", "Turkmen"]
               },
               {
                   "name": "Antigua and Barbuda",
                   "languages": ["English"]
               },
               {
                   "name": "Anguilla",
                   "languages": ["English"]
               },
               // ...
           ]
       }
   }
   ```

### 2. Mendapatkan Data Lengkap Sebuah Negara

- Endpoint: GET http://localhost:5000/countries/{KODE_NEGARA}

   (Gantilah `{KODE_NEGARA}` dengan kode negara yang sesuai, misalnya "AE" untuk United Arab Emirates.)

- Contoh Response JSON:

   ```json
   {
     "data": {
       "country": {
         "awsRegion": "me-south-1",
         "capital": "Abu Dhabi",
         "code": "AE",
         "currencies": [
           "AED"
         ],
         "currency": "AED",
         "emoji": "🇦🇪",
         "emojiU": "U+1F1E6 U+1F1EA",
         "name": "United Arab Emirates",
         "native": "دولة الإمارات العربية المتحدة",
         "phone": "971",
         "phones": [
           "971"
         ]
       }
     }
   }
   ```
