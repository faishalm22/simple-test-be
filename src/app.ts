import express from 'express';
import countriesRouter from './routes/countries';

const app = express();
const port = 5000;

app.use('/', countriesRouter);

app.listen(port, () => {
  console.log(`Server berjalan di http://localhost:${port}`);
});
