import express from 'express';
import * as countriesController from '../controllers/countriesController';

const router = express.Router();

// Rute untuk mendapatkan daftar negara
router.get('/countries', countriesController.getCountries);

// Rute untuk mendapatkan data lengkap sebuah negara berdasarkan kode negara (misalnya, "AE")
router.get('/countries/:countryCode', countriesController.getCountryByCode);

export default router;
