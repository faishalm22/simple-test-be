import { Request, Response } from 'express';
import axios from 'axios';

const graphqlEndpoint = 'https://countries.trevorblades.com/graphql';

// fungsi untuk mendapatkan daftar negara
export const getCountries = async (_req: Request, res: Response) => {
  try {
    const response = await axios.post(graphqlEndpoint, {
      query: `
        {
          countries {
            name
            languages {
              name
            }
          }
        }
      `,
    });

    const countriesData = response.data.data.countries.map((country: any) => ({
      name: country.name,
      languages: country.languages.map((language: any) => language.name),
    }));

    res.json({ data: { countries: countriesData } });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Gagal mengambil data negara dari GraphQL endpoint' });
  }
};

// fungsi untuk mendapatkan data lengkap sebuah negara berdasarkan kode negara (misalnya, "AE")
export const getCountryByCode = async (req: Request, res: Response) => {
  const { countryCode } = req.params;

  try {
    const response = await axios.post(graphqlEndpoint, {
      query: `
        {
          country(code: "${countryCode}") {
            awsRegion
            capital
            code
            currencies
            currency
            emoji
            emojiU
            name
            native
            phone
            phones
          }
        }
      `
    });
  
    const countryData = response.data;

    if (countryData) {
      res.json(countryData);
    } else {

      res.status(404).json({ error: 'Negara tidak ditemukan' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Gagal mengambil data negara dari GraphQL endpoint' });
  }
};

